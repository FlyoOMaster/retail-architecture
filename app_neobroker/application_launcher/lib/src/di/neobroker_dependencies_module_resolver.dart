import 'package:di/di.dart';
import 'package:environment/environment_lib.dart';
import 'package:feature_product_hub_api/feature_product_hub_api.dart';
import 'package:feature_product_hub_impl/feature_product_hub_impl.dart';
import 'package:feature_stocks_api/feature_stocks_api.dart';
import 'package:feature_stocks_ui_mobile/feature_stocks_ui_mobile.dart';
import 'package:neobroker_feature_welcome_screen_api/feature_welcome_screen_api.dart';
import 'package:navigation/navigation.dart';
import 'package:neobroker_feature_welcome_screen_ui_mobile/neobroker_feature_welcome_screen_ui_mobile.dart';

class NeobrokerDependenciesModuleResolver {
  static register({required Environment environment, required Function productHubRegister}) {
    DependencyProvider.registerLazySingleton(() => environment, instanceName: ApplicationType.neobroker.id);
    var isEmbedded = environment.applicationWorkType == ApplicationWorkType.embedded;
    _registerFeatures(isEmbedded, productHubRegister);
    _registerNavigation(isEmbedded);
  }

  static _registerFeatures(bool isEmbedded, Function productHubRegister) {
    NeobrokerWelcomeScreenFeatureDependencyModule.register();
    StocksFeatureDependencyModule.register();

    productHubRegister();
  }

  static _registerNavigation(bool isEmbedded) {

    var features = [
      NeobrokerWelcomeScreenFeatureConfig.NAME,
      StocksFeatureConfig.NAME,
      ProductHubFeatureConfig.NAME
    ];

    _registerNavigationFeatures(features);
  }

  static _registerNavigationFeatures(List<String> names) {
    DependencyProvider.registerLazySingleton(() {
      final neobrokerNavigationRouter = ApplicationNavigatorConfig(
          ApplicationType.neobroker.id,
          Map<String, NavigationRouter>.fromIterable(
            names,
            key: (v) => v,
            value: (v) =>
                DependencyProvider.get<NavigationRouter>(instanceName: v),
          ));

      return neobrokerNavigationRouter;
    }, instanceName: ApplicationType.neobroker.id);
  }
}
