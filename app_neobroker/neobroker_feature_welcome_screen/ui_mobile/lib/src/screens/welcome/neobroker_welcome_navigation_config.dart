import 'package:neobroker_feature_welcome_screen_api/feature_welcome_screen_api.dart';
import 'package:navigation/navigation.dart';

class NeobrokerWelcomeNavigationConfig extends ScreenNavigationConfig {
  NeobrokerWelcomeNavigationConfig()
      : super(
          id: 'welcome',
          feature: NeobrokerWelcomeScreenFeatureConfig.NAME,
        );
}
