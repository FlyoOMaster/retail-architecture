import 'package:environment/environment_lib.dart';
import 'package:feature_product_hub_api/feature_product_hub_api.dart';
import 'package:feature_stocks_api/feature_stocks_api.dart';
import 'package:navigation/navigation.dart';
import 'package:ui/cubit/base_cubit.dart';

class NeobrokerWelcomeCubit extends BaseCubit<bool> {
  final ApplicationNavigationProvider applicationNavigator;
  final NavigationProvider navigator;
  final Environment environment;

  NeobrokerWelcomeCubit({
    required this.navigator,
    required this.applicationNavigator,
    required this.environment,
  }) : super(false) {
    emit(environment.applicationWorkType == ApplicationWorkType.embedded);
  }

  onOnboardingClick() {
    navigator.navigateTo(StocksFeatureConfig());
  }

  onPressed() async {
    // final result = await DependencyProvider.get<NavigationProvider>().navigateTo(ProductHubFeatureConfig());
    // if(result == 'Yas mall') {
    //   emit(state + 1);
    // }
    // applicationNavigator.navigateToApplication(
    //   applicationType: ApplicationType.retail,
    //   featureConfig: WelcomeScreenFeatureConfig(),
    // );

    // navigator.goBack("HAHA: data returned from the different app");
    navigator.navigateTo(ProductHubFeatureConfig(ProductHubFeatureArgs(currentApplication: ProductHubFeatureApplicationType.neobroker)));
  }
}
