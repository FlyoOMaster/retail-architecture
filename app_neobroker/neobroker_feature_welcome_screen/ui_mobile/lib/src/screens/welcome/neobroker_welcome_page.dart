import 'package:di/di.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui/page/base_page.dart';

import 'neobroker_welcome_cubit.dart';

class NeobrokerWelcomePage extends BasePage<bool, NeobrokerWelcomeCubit> {
  const NeobrokerWelcomePage({Key? key}) : super(key: key);


  @override
  NeobrokerWelcomeCubit createBloc() {
    return DependencyProvider.get<NeobrokerWelcomeCubit>();
  }

  @override
  Widget buildPage(BuildContext context, NeobrokerWelcomeCubit cubit, bool productHubAvailable) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        title: Text('Neobroker Welcome screen'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(onPressed: cubit.onOnboardingClick, child: Text('To stocks')),
          ],
        ),
      ),
      floatingActionButton: productHubAvailable ? FloatingActionButton(
        onPressed: cubit.onPressed,
        tooltip: 'Product hub',
        child: Icon(Icons.account_tree_rounded),
      ) : null,// This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
