import 'package:neobroker_feature_welcome_screen_api/feature_welcome_screen_api.dart';
import 'package:neobroker_feature_welcome_screen_ui_mobile/src/screens/welcome/neobroker_welcome_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:navigation/navigation.dart';

class NeobrokerWelcomeRouter extends NavigationRouter {
  @override
  Route getScreenRoute(RouteSettings settings) {
    final config = settings.arguments;
    switch(config.runtimeType) {
      case NeobrokerWelcomeScreenFeatureConfig:
      case NeobrokerWelcomeScreenFeatureConfig:
        return toRoute(NeobrokerWelcomePage());

      default: throw Exception('Unknown $config for the ${this.runtimeType}');
    }
  }

}

extension _RouteWrapper on Widget {
  MaterialPageRoute toRoute() {
    return MaterialPageRoute(builder: (BuildContext context) => this);
  }
}