import 'package:di/di.dart';
import 'package:neobroker_feature_welcome_screen_api/feature_welcome_screen_api.dart';
import 'package:neobroker_feature_welcome_screen_ui_mobile/src/navigation/neobroker_welcome_router.dart';
import 'package:navigation/navigation.dart';
import 'package:neobroker_feature_welcome_screen_ui_mobile/src/screens/welcome/neobroker_welcome_cubit.dart';

class NeobrokerWelcomeScreenFeatureDependencyModule {
  static register() {
    DependencyProvider.registerLazySingleton(() => NeobrokerWelcomeRouter());

    DependencyProvider.registerLazySingleton<NavigationRouter>(
      () => DependencyProvider.get<NeobrokerWelcomeRouter>(),
      instanceName: NeobrokerWelcomeScreenFeatureConfig.NAME,
    );

    _cubits();
  }

  static _cubits() {
    DependencyProvider.registerFactory(() => NeobrokerWelcomeCubit(
        navigator: DependencyProvider.get(),
        applicationNavigator: DependencyProvider.get(),
        environment: DependencyProvider.get(
          instanceName: ApplicationType.neobroker.id,
        )));
  }
}
