import 'package:navigation/navigation.dart';

class NeobrokerWelcomeScreenFeatureConfig extends FeatureNavigationConfig {
  NeobrokerWelcomeScreenFeatureConfig() : super(NAME);

  static const NAME = 'neobroker_welcome_screen';
}