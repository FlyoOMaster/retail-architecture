import 'package:feature_stocks_api/feature_stocks_api.dart';
import 'package:feature_stocks_ui_mobile/src/screens/stock_list/stock_list_navigation_config.dart';
import 'package:feature_stocks_ui_mobile/src/screens/stock_list/stock_list_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:navigation/navigation.dart';

class StocksRouter extends NavigationRouter {
  @override
  Route getScreenRoute(RouteSettings settings) {
    final config = settings.arguments;
    switch(config.runtimeType) {
      case StocksFeatureConfig:
      case StockListNavigationConfig:
        return toRoute(StockListPage());

      default: throw Exception('Unknown $config for the ${this.runtimeType}');
    }
  }
}