import 'package:di/di.dart';
import 'package:feature_stocks_api/feature_stocks_api.dart';
import 'package:feature_stocks_ui_mobile/src/navigation/stocks_router.dart';
import 'package:feature_stocks_ui_mobile/src/screens/stock_list/stock_list_cubit.dart';
import 'package:navigation/navigation.dart';

class StocksFeatureDependencyModule {
  static register() {
    DependencyProvider.registerLazySingleton(() => StocksRouter());

    DependencyProvider.registerLazySingleton<NavigationRouter>(
      () => DependencyProvider.get<StocksRouter>(),
      instanceName: StocksFeatureConfig.NAME,
    );

    _cubits();
  }

  static _cubits() {
    DependencyProvider.registerFactory(() => StockListCubit(
          navigator: DependencyProvider.get(),
          applicationNavigator: DependencyProvider.get()
        ));
  }
}
