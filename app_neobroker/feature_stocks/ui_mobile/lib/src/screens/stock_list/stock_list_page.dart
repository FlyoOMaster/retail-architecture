import 'package:di/di.dart';
import 'package:feature_stocks_ui_mobile/src/screens/stock_list/stock_list_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui/page/base_page.dart';
class StockListPage extends BasePage<int, StockListCubit> {
  const StockListPage({Key? key}) : super(key: key);

  @override
  StockListCubit createBloc() {
    return DependencyProvider.get<StockListCubit>();
  }

  @override
  Widget buildPage(BuildContext context, StockListCubit cubit, int state) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        title: Text('Neobroker stock screen'),
      ),
      body: Center(
        child: Text(
            'You are inside io Neobroker stock application'
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
