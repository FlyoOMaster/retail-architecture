import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:navigation/navigation.dart';
import 'package:ui/cubit/base_cubit.dart';

class StockListCubit extends BaseCubit<int> {
  final ApplicationNavigationProvider applicationNavigator;
  final NavigationProvider navigator;

  StockListCubit({
    required this.navigator,
    required this.applicationNavigator,
  }) : super(0);
}
