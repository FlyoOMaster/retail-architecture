import 'package:feature_stocks_api/feature_stocks_api.dart';
import 'package:navigation/navigation.dart';

class StockListNavigationConfig extends ScreenNavigationConfig {
  StockListNavigationConfig()
      : super(
          id: 'onboarding',
          feature: StocksFeatureConfig.NAME,
        );
}
