import 'package:navigation/navigation.dart';

class StocksFeatureConfig extends FeatureNavigationConfig {
  StocksFeatureConfig() : super(NAME);

  static const NAME = 'stocks';
}