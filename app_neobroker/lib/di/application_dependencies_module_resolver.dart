import 'package:app_neobroker/global/global_keys.dart';
import 'package:app_neobroker/navigation/retail_application_navigator_router.dart';
import 'package:di/di.dart';
import 'package:environment/environment_lib.dart';
import 'package:feature_onboarding_api/feature_onboarding_api.dart';
import 'package:feature_onboarding_ui_mobile/feature_onboarding_ui_mobile.dart';
import 'package:feature_product_hub_impl/feature_product_hub_impl.dart';
import 'package:feature_stocks_api/feature_stocks_api.dart';
import 'package:feature_stocks_ui_mobile/feature_stocks_ui_mobile.dart';
import 'package:flutter/widgets.dart';
import 'package:navigation/navigation.dart';
import 'package:neobroker_application_launcher/neobroker_application_launcher.dart';
import 'package:neobroker_feature_welcome_screen_api/feature_welcome_screen_api.dart';
import 'package:neobroker_feature_welcome_screen_ui_mobile/neobroker_feature_welcome_screen_ui_mobile.dart';

class ApplicationDependenciesModuleResolver {
  static register() {
    _registerApplication();
    _registerFeatures();
    _registerNavigation();
  }

  static _registerApplication() {
    DependencyProvider.registerLazySingleton<NavigatorState>(
      () => GlobalKeys.navigatorKey.currentState!,
    );

    NeobrokerDependenciesModuleResolver.register(
      environment:
          Environment(applicationWorkType: ApplicationWorkType.standalone),
      productHubRegister: ProductHubFeatureDependencyModuleResolver.register,
    );
  }

  static _registerFeatures() {
    OnboardingFeatureDependencyModule.register();
  }

  static _registerNavigation() {
    DependencyProvider.registerLazySingleton(
      () => NavigationProviderImpl(
        navigator: DependencyProvider.get(),
      ),
    );

    DependencyProvider.registerLazySingleton<NavigationProvider>(
      () => DependencyProvider.get<NavigationProviderImpl>(),
    );

    DependencyProvider.registerLazySingleton<ApplicationNavigationProvider>(
      () => DependencyProvider.get<NavigationProviderImpl>(),
    );

    _registerNavigationFeatures([
      OnboardingFeatureConfig.NAME,
      NeobrokerWelcomeScreenFeatureConfig.NAME,
      StocksFeatureConfig.NAME,
    ]);
  }

  static _registerNavigationFeatures(List<String> names) {
    final applicationFeatures = Map<String, NavigationRouter>.fromIterable(
      names,
      key: (v) => v,
      value: (v) => DependencyProvider.get<NavigationRouter>(instanceName: v),
    );

    final embeddedFeatures = DependencyProvider.get<ApplicationNavigatorConfig>(
            instanceName: ApplicationType.neobroker.id)
        .features;

    DependencyProvider.registerLazySingleton(() {
      final neobrokerApplicationNavigatorConfig = ApplicationNavigatorConfig(
          ApplicationType.neobroker.id,
          {...applicationFeatures, ...embeddedFeatures});

      return NeobrokerApplicationNavigatorRouter(
        initialApplicationId: ApplicationType.neobroker.id,
        applications: [neobrokerApplicationNavigatorConfig],
      );
    });
  }
}
