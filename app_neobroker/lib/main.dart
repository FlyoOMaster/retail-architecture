import 'package:app_neobroker/navigation/retail_application_navigator_router.dart';
import 'package:di/di.dart';
import 'package:flutter/material.dart';

import 'di/application_dependencies_module_resolver.dart';
import 'neobroker_application.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  ApplicationDependenciesModuleResolver.register();

  runApp(
    NeobrokerApplication(
      applicationNavigator: DependencyProvider.get<NeobrokerApplicationNavigatorRouter>(),
    ),
  );
}
