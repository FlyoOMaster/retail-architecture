import 'package:feature_onboarding_api/feature_onboarding_api.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:navigation/navigation.dart';

class NeobrokerApplicationNavigatorRouter extends ApplicationNavigatorRouter {
  NeobrokerApplicationNavigatorRouter({
    required String initialApplicationId,
    required List<ApplicationNavigatorConfig> applications,
  }) : super(
          initialApplicationId: initialApplicationId,
          applicationNavigatorConfigs: applications,
        );

  @override
  Route getInitialRoute() {
    var onboardingFeatureConfig = OnboardingFeatureConfig();
    return getFeatureRouter(onboardingFeatureConfig).getScreenRoute(
        RouteSettings(name: '/', arguments: onboardingFeatureConfig));
  }
}
