import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'global/global_keys.dart';
import 'navigation/retail_application_navigator_router.dart';

class NeobrokerApplication extends StatelessWidget {
  final NeobrokerApplicationNavigatorRouter applicationNavigator;

  const NeobrokerApplication({required this.applicationNavigator}) : super();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Neobroker Demo',
      onGenerateRoute: applicationNavigator.getRoute,
      initialRoute: '/',
      navigatorKey: GlobalKeys.navigatorKey,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}
