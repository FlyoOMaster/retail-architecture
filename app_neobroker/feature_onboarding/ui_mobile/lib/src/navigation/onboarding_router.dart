import 'package:feature_onboarding_api/feature_onboarding_api.dart';
import 'package:feature_onboarding_ui_mobile/src/screens/welcome/onboarding_navigation_config.dart';
import 'package:feature_onboarding_ui_mobile/src/screens/welcome/onboarding_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:navigation/navigation.dart';

class OnboardingRouter extends NavigationRouter {
  @override
  Route getScreenRoute(RouteSettings settings) {
    final config = settings.arguments;
    switch(config.runtimeType) {
      case OnboardingFeatureConfig:
      case OnboardingNavigationConfig:
        return toRoute(OnboardingPage());

      default: throw Exception('Unknown $config for the ${this.runtimeType}');
    }
  }
}