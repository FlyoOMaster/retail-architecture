import 'package:di/di.dart';
import 'package:feature_onboarding_api/feature_onboarding_api.dart';
import 'package:feature_onboarding_ui_mobile/src/navigation/onboarding_router.dart';
import 'package:feature_onboarding_ui_mobile/src/screens/welcome/onboarding_cubit.dart';
import 'package:navigation/navigation.dart';

class OnboardingFeatureDependencyModule {
  static register() {
    DependencyProvider.registerLazySingleton(() => OnboardingRouter());

    DependencyProvider.registerLazySingleton<NavigationRouter>(
      () => DependencyProvider.get<OnboardingRouter>(),
      instanceName: OnboardingFeatureConfig.NAME,
    );

    _cubits();
  }

  static _cubits() {
    DependencyProvider.registerFactory(() => OnboardingCubit(
          navigator: DependencyProvider.get(),
          applicationNavigator: DependencyProvider.get()
        ));
  }
}
