import 'package:di/di.dart';
import 'package:feature_onboarding_ui_mobile/src/screens/welcome/onboarding_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui/page/base_page.dart';

class OnboardingPage extends BasePage<int, OnboardingCubit> {
  const OnboardingPage({Key? key}) : super(key: key);

  @override
  OnboardingCubit createBloc() {
    return DependencyProvider.get<OnboardingCubit>();
  }

  @override
  Widget buildPage(BuildContext context, OnboardingCubit cubit, int state) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        title: Text('Neobroker Onboarding screen'),
      ),
      body: Center(
        child: Text(
            'You are inside io Neobroker Onboarding application'
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: context.read<OnboardingCubit>().onPressed,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
