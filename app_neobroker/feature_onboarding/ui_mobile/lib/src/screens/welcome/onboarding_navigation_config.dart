import 'package:feature_onboarding_api/feature_onboarding_api.dart';
import 'package:navigation/navigation.dart';

class OnboardingNavigationConfig extends ScreenNavigationConfig {
  OnboardingNavigationConfig()
      : super(
          id: 'onboarding',
          feature: OnboardingFeatureConfig.NAME,
        );
}
