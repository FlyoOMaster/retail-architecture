import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:navigation/navigation.dart';
import 'package:neobroker_feature_welcome_screen_api/feature_welcome_screen_api.dart';
import 'package:ui/cubit/base_cubit.dart';

class OnboardingCubit extends BaseCubit<int> {
  final ApplicationNavigationProvider applicationNavigator;
  final NavigationProvider navigator;

  OnboardingCubit({
    required this.navigator,
    required this.applicationNavigator,
  }) : super(0);

  onPressed() async {
    navigator.navigateTo(NeobrokerWelcomeScreenFeatureConfig());
  }
}
