import 'package:navigation/navigation.dart';

class OnboardingFeatureConfig extends FeatureNavigationConfig {
  OnboardingFeatureConfig() : super(NAME);

  static const NAME = 'neobroker_onboarding';
}