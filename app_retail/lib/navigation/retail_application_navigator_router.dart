import 'package:feature_welcome_screen_api/feature_welcome_screen_api.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:navigation/navigation.dart';

class RetailApplicationNavigatorRouter extends ApplicationNavigatorRouter {
  RetailApplicationNavigatorRouter({
    required List<ApplicationNavigatorConfig> applications,
  }) : super(
          initialApplicationId: ApplicationType.retail.id,
          applicationNavigatorConfigs: applications,
        );

  @override
  Route getInitialRoute() {
    var welcomeScreenFeatureConfig = WelcomeScreenFeatureConfig();
    return getFeatureRouter(welcomeScreenFeatureConfig).getScreenRoute(
        RouteSettings(name: '/', arguments: welcomeScreenFeatureConfig));
  }
}
