import 'package:app_retail/global/global_keys.dart';
import 'package:app_retail/navigation/retail_application_navigator_router.dart';
import 'package:di/di.dart';
import 'package:environment/environment_lib.dart';
import 'package:feature_account_api/feature_account_api.dart';
import 'package:feature_account_ui_mobile/feature_account_ui_mobile.dart';
import 'package:feature_product_hub_api/feature_product_hub_api.dart';
import 'package:feature_product_hub_impl/feature_product_hub_impl.dart';
import 'package:feature_transactions_api/feature_transactions_api.dart';
import 'package:feature_transactions_impl/feature_transactions_impl.dart';
import 'package:feature_transactions_ui_mobile/feature_transactions_ui_mobile.dart';
import 'package:feature_welcome_screen_api/feature_welcome_screen_api.dart';
import 'package:feature_welcome_screen_ui_mobile/feature_welcome_screen_ui_mobile.dart';
import 'package:flutter/widgets.dart';
import 'package:navigation/navigation.dart';
import 'package:neobroker_application_launcher/neobroker_application_launcher.dart';

class ApplicationDependenciesModuleResolver {
  static register() {
    _registerApplication();
    _registerFeatures();
    _registerNavigation();
  }

  static _registerApplication() {
    DependencyProvider.registerLazySingleton(
        () => Environment(applicationWorkType: ApplicationWorkType.host),
        instanceName: ApplicationType.retail.id);

    DependencyProvider.registerLazySingleton<NavigatorState>(
      () => GlobalKeys.navigatorKey.currentState!,
    );

    _embedded();
  }

  static _embedded() {
    NeobrokerDependenciesModuleResolver.register(
      environment:
          Environment(applicationWorkType: ApplicationWorkType.embedded),
      productHubRegister: ProductHubFeatureDependencyModuleResolver.register,
    );
  }

  static _registerFeatures() {
    WelcomeScreenFeatureDependencyModuleResolver.register();
    AccountFeatureDependencyModuleResover.register();
    TransactionsDomainDependencyModuleResolver.register();
    TransactionsFeatureDependencyModuleResolver.register();
  }

  static _registerNavigation() {
    DependencyProvider.registerLazySingleton(
      () => NavigationProviderImpl(
        navigator: DependencyProvider.get(),
      ),
    );

    DependencyProvider.registerLazySingleton<NavigationProvider>(
      () => DependencyProvider.get<NavigationProviderImpl>(),
    );

    DependencyProvider.registerLazySingleton<ApplicationNavigationProvider>(
      () => DependencyProvider.get<NavigationProviderImpl>(),
    );

    _registerNavigationFeatures([
      ProductHubFeatureConfig.NAME,
      WelcomeScreenFeatureConfig.NAME,
      AccountFeatureConfig.NAME,
      TransactionsFeatureNavigationConfig.NAME
    ]);
  }

  static _registerNavigationFeatures(List<String> names) {
    DependencyProvider.registerLazySingleton(
        () => ApplicationNavigatorConfig(
            ApplicationType.retail.id,
            Map<String, NavigationRouter>.fromIterable(
              names,
              key: (v) => v,
              value: (v) =>
                  DependencyProvider.get<NavigationRouter>(instanceName: v),
            )),
        instanceName: ApplicationType.retail.id);

    DependencyProvider.registerLazySingleton(() {
      final retailApplicationNavigatorConfig =
          DependencyProvider.get<ApplicationNavigatorConfig>(
              instanceName: ApplicationType.retail.id);

      final neobrokerApplicationNavigatorConfig =
          DependencyProvider.get<ApplicationNavigatorConfig>(
              instanceName: ApplicationType.neobroker.id);

      return RetailApplicationNavigatorRouter(
        applications: [
          retailApplicationNavigatorConfig,
          neobrokerApplicationNavigatorConfig
        ],
      );
    });
  }
}
