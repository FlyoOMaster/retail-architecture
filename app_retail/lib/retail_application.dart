import 'package:app_retail/navigation/retail_application_navigator_router.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'global/global_keys.dart';

class RetailApplication extends StatelessWidget {
  final RetailApplicationNavigatorRouter applicationNavigator;

  const RetailApplication({required this.applicationNavigator}) : super();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Retail Demo',
      onGenerateRoute: applicationNavigator.getRoute,
      initialRoute: '/',
      navigatorKey: GlobalKeys.navigatorKey,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}
