import 'package:app_retail/di/application_dependencies_module_resolver.dart';
import 'package:app_retail/navigation/retail_application_navigator_router.dart';
import 'package:app_retail/retail_application.dart';
import 'package:di/di.dart';
import 'package:flutter/material.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  ApplicationDependenciesModuleResolver.register();

  runApp(
    RetailApplication(
      applicationNavigator: DependencyProvider.get<RetailApplicationNavigatorRouter>(),
    ),
  );
}
