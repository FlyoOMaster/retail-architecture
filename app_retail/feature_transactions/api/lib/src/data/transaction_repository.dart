import 'dart:core';

abstract class TransactionRepository {
  Future<List<String>> fetchTransactions();
  Future<List<String>> fetchLastTransactions(int count);
}