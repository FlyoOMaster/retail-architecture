import 'package:navigation/navigation.dart';

class TransactionsFeatureNavigationConfig extends FeatureNavigationConfig {
  TransactionsFeatureNavigationConfig() : super(NAME);

  static const NAME = 'transactions';
}