import 'package:domain/usecase/base_uscase.dart';

abstract class FetchLastTransactionsUseCase extends FutureUseCase<int, List<String>> {}