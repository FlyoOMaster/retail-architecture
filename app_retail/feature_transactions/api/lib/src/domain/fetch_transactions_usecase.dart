import 'package:domain/usecase/base_uscase.dart';

abstract class FetchTransactionsUseCase extends SimpleFutureUseCase<List<String>> {}