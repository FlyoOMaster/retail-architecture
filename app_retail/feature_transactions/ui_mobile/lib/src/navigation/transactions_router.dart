import 'package:feature_transactions_api/feature_transactions_api.dart';
import 'package:feature_transactions_ui_mobile/src/screens/transaction_list_navigation_config.dart';
import 'package:feature_transactions_ui_mobile/src/screens/transaction_list_page.dart';
import 'package:flutter/widgets.dart';
import 'package:navigation/navigation.dart';

class TransactionsRouter extends NavigationRouter {
  @override
  Route getScreenRoute(RouteSettings settings) {
    final config = settings.arguments;
    switch(config.runtimeType) {
      case TransactionsFeatureNavigationConfig:
      case TransactionListNavigationConfig:
        return toRoute(TransactionListPage());

      default: throw Exception('Unknown $config for the ${this.runtimeType}');
    }
  }

}