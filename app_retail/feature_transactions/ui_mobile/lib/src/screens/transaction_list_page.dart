import 'package:di/di.dart';
import 'package:feature_transactions_ui_mobile/src/screens/transaction_list_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ui/page/base_page.dart';

class TransactionListPage extends BasePage<List<String>, TransactionListCubit> {
  @override
  TransactionListCubit createBloc() {
    return DependencyProvider.get<TransactionListCubit>();
  }

  @override
  Widget buildPage(
      BuildContext context, TransactionListCubit cubit, List<String> state) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Transaction list'),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: state.length,
              itemBuilder: (_, i) {
                return Text(state[i]);
              },
            ),
          ),
          ElevatedButton(onPressed: cubit.onAccountClick, child: Text('Check account'))
        ],
      ),
    );
  }
}
