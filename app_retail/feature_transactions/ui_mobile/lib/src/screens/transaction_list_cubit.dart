import 'package:feature_account_api/feature_account_api.dart';
import 'package:feature_transactions_api/feature_transactions_api.dart';
import 'package:navigation/navigation.dart';
import 'package:ui/cubit/base_cubit.dart';

class TransactionListCubit extends BaseCubit<List<String>> {
  final NavigationProvider navigator;
  final FetchTransactionsUseCase fetchTransactions;

  TransactionListCubit({
    required this.navigator,
    required this.fetchTransactions,
  }) : super([]) {
    _fetchTransactions();
  }

  onAccountClick() {
    navigator.navigateTo(AccountFeatureConfig());
  }

  _fetchTransactions() async {
    emit(await fetchTransactions());
  }
}
