import 'package:feature_transactions_api/feature_transactions_api.dart';
import 'package:navigation/navigation.dart';

class TransactionListNavigationConfig extends ScreenNavigationConfig {
  TransactionListNavigationConfig() : super(
    id: 'list',
    feature: TransactionsFeatureNavigationConfig.NAME
  );

}