import 'package:di/di.dart';
import 'package:feature_transactions_api/feature_transactions_api.dart';
import 'package:feature_transactions_ui_mobile/src/navigation/transactions_router.dart';
import 'package:feature_transactions_ui_mobile/src/screens/transaction_list_cubit.dart';
import 'package:navigation/navigation.dart';

class TransactionsFeatureDependencyModuleResolver {
  static register() {
    DependencyProvider.registerLazySingleton(() => TransactionsRouter());

    DependencyProvider.registerLazySingleton<NavigationRouter>(
      () => DependencyProvider.get<TransactionsRouter>(),
      instanceName: TransactionsFeatureNavigationConfig.NAME,
    );

    _cubits();
  }

  static _cubits() {
    DependencyProvider.registerFactory(
      () => TransactionListCubit(
          navigator: DependencyProvider.get<NavigationProvider>(),
          fetchTransactions: DependencyProvider.get()),
    );
  }
}
