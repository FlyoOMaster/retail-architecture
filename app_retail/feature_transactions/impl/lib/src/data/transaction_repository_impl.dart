import 'dart:core';

import 'package:feature_transactions_api/feature_transactions_api.dart';

class TransactionRepositoryImpl implements TransactionRepository {
  List<String> _transactionSource = [
    'Transaction 1',
    'Transaction 2',
    'Transaction 3',
    'Transaction 4',
    'Transaction 5',
    'Transaction 6',
    'Transaction 7',
    'Transaction 8',
  ];

  @override
  Future<List<String>> fetchLastTransactions(int count) {
    return Future.value(_transactionSource.skip(_transactionSource.length - count).toList());
  }

  @override
  Future<List<String>> fetchTransactions() {
    return Future.value(_transactionSource);
  }

}