import 'package:feature_transactions_api/feature_transactions_api.dart';

class FetchLastTransactionsUseCaseImpl implements FetchLastTransactionsUseCase {
  final TransactionRepository transactionRepository;

  FetchLastTransactionsUseCaseImpl({required this.transactionRepository});

  @override
  Future<List<String>> call(int params) {
    return transactionRepository.fetchLastTransactions(params);
  }

}