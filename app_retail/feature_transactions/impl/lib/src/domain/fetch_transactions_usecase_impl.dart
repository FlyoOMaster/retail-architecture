import 'package:feature_transactions_api/feature_transactions_api.dart';

class FetchTransactionsUseCaseImpl implements FetchTransactionsUseCase {
  final TransactionRepository transactionRepository;

  FetchTransactionsUseCaseImpl({required this.transactionRepository});

  @override
  Future<List<String>> call() {
    return transactionRepository.fetchTransactions();
  }



}