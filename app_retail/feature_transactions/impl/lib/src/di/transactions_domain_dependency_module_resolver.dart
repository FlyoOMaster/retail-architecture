import 'package:di/di.dart';
import 'package:feature_transactions_api/feature_transactions_api.dart';
import 'package:feature_transactions_impl/src/data/transaction_repository_impl.dart';
import 'package:feature_transactions_impl/src/domain/fetch_last_transactions_usecase_impl.dart';
import 'package:feature_transactions_impl/src/domain/fetch_transactions_usecase_impl.dart';

class TransactionsDomainDependencyModuleResolver {
  static register() {
    _repository();
    _domain();
  }

  static _repository() {
    DependencyProvider.registerLazySingleton<TransactionRepository>(
            () => TransactionRepositoryImpl());
  }

  static _domain() {
    DependencyProvider.registerFactory<FetchTransactionsUseCase>(
          () => FetchTransactionsUseCaseImpl(
          transactionRepository: DependencyProvider.get()),
    );

    DependencyProvider.registerFactory<FetchLastTransactionsUseCase>(
          () => FetchLastTransactionsUseCaseImpl(
          transactionRepository: DependencyProvider.get()),
    );
  }
}