import 'package:navigation/navigation.dart';

class WelcomeScreenFeatureConfig extends FeatureNavigationConfig {
  WelcomeScreenFeatureConfig() : super(NAME);

  static const NAME = 'welcome_screen';
}