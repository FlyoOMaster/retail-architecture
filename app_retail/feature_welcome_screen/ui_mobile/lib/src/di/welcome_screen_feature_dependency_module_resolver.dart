import 'package:di/di.dart';
import 'package:feature_welcome_screen_api/feature_welcome_screen_api.dart';
import 'package:feature_welcome_screen_ui_mobile/src/navigation/welcome_router.dart';
import 'package:feature_welcome_screen_ui_mobile/src/screens/welcome/welcome_cubit.dart';
import 'package:navigation/navigation.dart';

class WelcomeScreenFeatureDependencyModuleResolver {
  static register() {
    DependencyProvider.registerLazySingleton(() => WelcomeRouter());

    DependencyProvider.registerLazySingleton<NavigationRouter>(
      () => DependencyProvider.get<WelcomeRouter>(),
      instanceName: WelcomeScreenFeatureConfig.NAME,
    );

    _cubits();
  }

  static _cubits() {
    DependencyProvider.registerFactory(() => WelcomeCubit(
          navigator: DependencyProvider.get(),
          applicationNavigator: DependencyProvider.get(),
        ));
  }
}
