import 'package:di/di.dart';
import 'package:feature_account_api/feature_account_api.dart';
import 'package:feature_product_hub_api/feature_product_hub_api.dart';
import 'package:feature_transactions_api/feature_transactions_api.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:navigation/navigation.dart';
import 'package:neobroker_feature_welcome_screen_api/feature_welcome_screen_api.dart';
import 'package:ui/cubit/base_cubit.dart';

class WelcomeCubit extends BaseCubit<int> {
  final ApplicationNavigationProvider applicationNavigator;
  final NavigationProvider navigator;

  WelcomeCubit({required this.navigator, required this.applicationNavigator})
      : super(0);

  onAccountClick() {
    navigator.navigateTo(AccountFeatureConfig());
  }

  onTransactionClick() {
    navigator.navigateTo(TransactionsFeatureNavigationConfig());
  }

  onProductHubClick() async {
    // final result = await DependencyProvider.get<NavigationProvider>().navigateTo(ProductHubFeatureConfig());
    // if(result == 'Yas mall') {
    //   emit(state + 1);
    // }
    // final String result = await applicationNavigator.navigateToApplication<String>(
    //   applicationType: ApplicationType.neobroker,
    //   featureConfig: NeobrokerWelcomeScreenFeatureConfig(),
    // ) ?? 'Empty =(';
    //
    // print(result);
    navigator.navigateTo(ProductHubFeatureConfig(ProductHubFeatureArgs(currentApplication: ProductHubFeatureApplicationType.retail)));
  }
}
