import 'package:di/di.dart';
import 'package:feature_welcome_screen_ui_mobile/src/screens/welcome/welcome_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui/page/base_page.dart';

class WelcomePage extends BasePage<int, WelcomeCubit> {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  WelcomeCubit createBloc() {
    return DependencyProvider.get<WelcomeCubit>();
  }

  @override
  Widget buildPage(BuildContext context, WelcomeCubit cubit, int state) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Retail Welcome screen'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(onPressed: cubit.onAccountClick, child: Text('Account')),
            ElevatedButton(onPressed: cubit.onTransactionClick, child: Text('Transactions')),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: cubit.onProductHubClick,
        tooltip: 'Product hub',
        child: Icon(Icons.account_tree_rounded),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
