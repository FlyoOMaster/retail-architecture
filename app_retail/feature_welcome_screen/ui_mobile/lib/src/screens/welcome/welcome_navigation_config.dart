import 'package:feature_welcome_screen_api/feature_welcome_screen_api.dart';
import 'package:navigation/navigation.dart';

class WelcomeNavigationConfig extends ScreenNavigationConfig {
  WelcomeNavigationConfig()
      : super(
          id: 'welcome',
          feature: WelcomeScreenFeatureConfig.NAME,
        );
}
