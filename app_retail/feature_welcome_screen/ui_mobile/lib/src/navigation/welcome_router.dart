import 'package:feature_welcome_screen_api/feature_welcome_screen_api.dart';
import 'package:feature_welcome_screen_ui_mobile/src/screens/welcome/welcome_navigation_config.dart';
import 'package:feature_welcome_screen_ui_mobile/src/screens/welcome/welcome_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:navigation/navigation.dart';

class WelcomeRouter extends NavigationRouter {
  @override
  Route getScreenRoute(RouteSettings settings) {
    final config = settings.arguments;
    switch(config.runtimeType) {
      case WelcomeScreenFeatureConfig:
      case WelcomeNavigationConfig:
        return toRoute(WelcomePage());

      default: throw Exception('Unknown $config for the ${this.runtimeType}');
    }
  }
}

