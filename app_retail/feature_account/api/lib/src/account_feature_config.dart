import 'package:navigation/navigation.dart';

class AccountFeatureConfig extends FeatureNavigationConfig {
  AccountFeatureConfig() : super(NAME);

  static const NAME = 'account';
}
