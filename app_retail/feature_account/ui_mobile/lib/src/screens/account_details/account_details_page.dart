import 'package:di/di.dart';
import 'package:feature_account_ui_mobile/src/screens/account_details/account_details_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ui/page/base_page.dart';

class AccountDetailsPage extends BasePage<List<String>, AccountDetailsCubit> {
  const AccountDetailsPage({Key? key}) : super(key: key);

  @override
  AccountDetailsCubit createBloc() {
    return DependencyProvider.get<AccountDetailsCubit>();
  }

  @override
  Widget buildPage(
      BuildContext context, AccountDetailsCubit cubit, List<String> state) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Account details'),
      ),
      body: Align(
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
                padding: EdgeInsets.only(top: 32),
                child: CircleAvatar(
                  child: Icon(Icons.person),
                  backgroundColor: Colors.grey,
                )),
            Text(
              'Personal info:',
              style: TextStyle(fontSize: 32),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('First name: '),
                SizedBox(
                  width: 30,
                ),
                Text('Ivan'),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Last name: '),
                SizedBox(
                  width: 30,
                ),
                Text('Ivanov'),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Job: '),
                SizedBox(
                  width: 30,
                ),
                Text('Developer'),
              ],
            ),
            Text(
              'Last transactions:',
              style: TextStyle(fontSize: 32),
            ),
            Container(
              width: double.infinity,
              decoration:
                  BoxDecoration(border: Border.all(color: Colors.black)),
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children:
                      state.map((transaction) => Text(transaction)).toList(),
                ),
              ),
            ),
            ElevatedButton(onPressed: cubit.onTransactionClick, child: Text('Go to transactions'))
          ],
        ),
      ),
    );
  }
}
