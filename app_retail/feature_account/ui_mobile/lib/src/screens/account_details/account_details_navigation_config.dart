import 'package:feature_account_api/feature_account_api.dart';
import 'package:navigation/navigation.dart';

class AccountDetailsNavigationConfig extends ScreenNavigationConfig {
  AccountDetailsNavigationConfig()
      : super(
          id: 'details',
          feature: AccountFeatureConfig.NAME,
        );
}