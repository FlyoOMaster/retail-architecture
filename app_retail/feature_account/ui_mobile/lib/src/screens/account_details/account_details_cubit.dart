import 'package:feature_transactions_api/feature_transactions_api.dart';
import 'package:navigation/navigation.dart';
import 'package:ui/cubit/base_cubit.dart';

class AccountDetailsCubit extends BaseCubit<List<String>> {
  final NavigationProvider navigator;
  final FetchLastTransactionsUseCase fetchLastTransactions;

  AccountDetailsCubit({
    required this.navigator,
    required this.fetchLastTransactions,
  }) : super([]) {
    _fetchLastTransactions();
  }

  _fetchLastTransactions() async {
    emit(await fetchLastTransactions(3));
  }

  onTransactionClick() {
    navigator.navigateTo(TransactionsFeatureNavigationConfig());
  }
}
