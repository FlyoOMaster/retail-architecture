import 'package:di/di.dart';
import 'package:feature_account_api/feature_account_api.dart';
import 'package:feature_account_ui_mobile/src/navigation/account_router.dart';
import 'package:feature_account_ui_mobile/src/screens/account_details/account_details_cubit.dart';
import 'package:feature_transactions_api/feature_transactions_api.dart';
import 'package:navigation/navigation.dart';

class AccountFeatureDependencyModuleResover {
  static register() {
    DependencyProvider.registerLazySingleton(() => AccountRouter());

    DependencyProvider.registerLazySingleton<NavigationRouter>(
      () => DependencyProvider.get<AccountRouter>(),
      instanceName: AccountFeatureConfig.NAME,
    );

    _cubits();
  }

  static _cubits() {
    DependencyProvider.registerFactory(() => AccountDetailsCubit(
          navigator: DependencyProvider.get<NavigationProvider>(),
          fetchLastTransactions:
              DependencyProvider.get<FetchLastTransactionsUseCase>(),
        ));
  }
}
