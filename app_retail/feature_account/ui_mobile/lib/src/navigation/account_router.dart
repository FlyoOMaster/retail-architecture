import 'package:feature_account_api/feature_account_api.dart';
import 'package:feature_account_ui_mobile/src/screens/account_details/account_details_navigation_config.dart';
import 'package:feature_account_ui_mobile/src/screens/account_details/account_details_page.dart';
import 'package:flutter/widgets.dart';
import 'package:navigation/navigation.dart';

class AccountRouter extends NavigationRouter {
  @override
  Route getScreenRoute(RouteSettings settings) {
    final config = settings.arguments;
    switch(config.runtimeType) {
      case AccountFeatureConfig:
      case AccountDetailsNavigationConfig:
        return toRoute(AccountDetailsPage());

      default: throw Exception('Unknown $config for the ${this.runtimeType}');
    }
  }

}