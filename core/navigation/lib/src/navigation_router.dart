import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

abstract class NavigationRouter {
  Route getScreenRoute(RouteSettings settings);

  MaterialPageRoute toRoute(Widget page) {
    return MaterialPageRoute(builder: (BuildContext context) => page);
  }
}