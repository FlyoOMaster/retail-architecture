import 'package:flutter/widgets.dart';
import 'package:navigation/navigation.dart';
import 'package:navigation/src/product/application_type.dart';
import 'package:navigation/src/screen/screen_navigation_config.dart';

abstract class NavigationProvider {
  Future<T?> navigateTo<T extends Object?>(
      FeatureNavigationConfig featureConfig);

  Future<T?> push<T extends Object?>(ScreenNavigationConfig config);

  backUntil(ScreenNavigationConfig config);

  replace(ScreenNavigationConfig config);

  goBack([Object? result]);
}

abstract class ApplicationNavigationProvider {
  Future<T?> navigateToApplication<T extends Object?>({
    required ApplicationType applicationType,
    required FeatureNavigationConfig featureConfig,
  });

  replaceApplication(
      ApplicationType applicationType, FeatureNavigationConfig config);
}

class NavigationProviderImpl
    implements ApplicationNavigationProvider, NavigationProvider {
  final NavigatorState navigator;

  NavigationProviderImpl({
    required this.navigator,
  });

  @override
  Future<T?> navigateToApplication<T extends Object?>({
    required ApplicationType applicationType,
    required FeatureNavigationConfig featureConfig,
  }) async {
    return navigator
        .pushNamed(
          _buildApplicationRouteName(applicationType, featureConfig),
          arguments: featureConfig,
        )
        .then((value) => value as T?);
  }

  @override
  Future<T?> navigateTo<T extends Object?>(
      FeatureNavigationConfig featureConfig) async {
    return navigator
        .pushNamed('${featureConfig.id}', arguments: featureConfig)
        .then((value) => value as T?);
  }

  @override
  Future<T?> push<T extends Object?>(ScreenNavigationConfig config) async {
    return navigator
        .pushNamed(_buildScreenRouteName(config), arguments: config)
        .then((value) => value as T?);
  }

  @override
  goBack([Object? result]) {
    navigator.pop(result);
  }

  @override
  backUntil(ScreenNavigationConfig config) {
    navigator.popUntil(ModalRoute.withName(_buildScreenRouteName(config)));
  }

  @override
  replace(ScreenNavigationConfig config) {
    navigator.popAndPushNamed(_buildScreenRouteName(config), arguments: config);
  }

  @override
  replaceApplication(
      ApplicationType applicationType, FeatureNavigationConfig config) {
    navigator.popUntil((route) => route.isFirst);
    navigator.popAndPushNamed(
        _buildApplicationRouteName(applicationType, config),
        arguments: config);
  }

  String _buildScreenRouteName(ScreenNavigationConfig config) {
    return '${config.feature}/${config.id}';
  }

  String _buildApplicationRouteName(
      ApplicationType applicationType, FeatureNavigationConfig featureConfig) {
    return '${applicationType.id}/${featureConfig.id}';
  }
}
