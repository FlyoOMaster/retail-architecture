import 'package:navigation/navigation.dart';

abstract class FeatureNavigationConfig extends NavigationConfig {
  FeatureNavigationConfig(String id) : super(id);
}