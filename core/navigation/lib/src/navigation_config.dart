abstract class NavigationConfig {
  final String id;

  NavigationConfig(this.id);

  @override
  bool operator ==(Object other) {
    if(this.runtimeType != other.runtimeType) {
      return false;
    }

    var config = (other as NavigationConfig);
    return id == config.id;
  }

  @override
  int get hashCode => id.hashCode;
}