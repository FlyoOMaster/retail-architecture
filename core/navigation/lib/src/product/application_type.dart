enum ApplicationType {
  retail, neobroker
}

extension ApplicationId on ApplicationType {
  String get id {
    switch(this) {
      case ApplicationType.retail:
        return 'retail';
      case ApplicationType.neobroker:
        return 'neobroker';
    }
  }
}