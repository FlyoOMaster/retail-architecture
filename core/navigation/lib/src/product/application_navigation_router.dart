import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:navigation/navigation.dart';

class ApplicationNavigatorConfig {
  final String applicationId;
  final Map<String, NavigationRouter> features;

  ApplicationNavigatorConfig(this.applicationId, this.features);
}

abstract class ApplicationNavigatorRouter {
  final String initialApplicationId;
  final List<ApplicationNavigatorConfig> applicationNavigatorConfigs;
  late ApplicationNavigatorConfig _currentApplicationNavigatorConfig;

  ApplicationNavigatorRouter({
    required this.initialApplicationId,
    required this.applicationNavigatorConfigs,
  }) {
    for(final config in applicationNavigatorConfigs) {
      if(config.applicationId == initialApplicationId) {
        _currentApplicationNavigatorConfig = config;
        return;
      }
    }

    throw Exception('Initial application id not contain inside of ApplicationNavigator: $initialApplicationId');
  }

  Route getInitialRoute();

  Route<dynamic>? getRoute(RouteSettings settings) {
    if (settings.name == INITIAL_ROUTE_ID) {
      return getInitialRoute();
    }

    final routeParts = settings.name?.split('/') ?? [];
    final isApplication =
        settings.arguments is FeatureNavigationConfig && routeParts.length == 2;

    var config = settings.arguments;
    if (config != null) {
      if (isApplication) {
        _currentApplicationNavigatorConfig = applicationNavigatorConfigs.firstWhere(
                (applicationConfig) =>
            applicationConfig.applicationId == routeParts.first,
            orElse: () => _currentApplicationNavigatorConfig);
      }

      return getFeatureRouter(config).getScreenRoute(settings);
    } else {
      throw Exception('Can`t find rout inside Retail app');
    }
  }

  @protected
  NavigationRouter getFeatureRouter(Object? config) {
    final applicationFeatures = _currentApplicationNavigatorConfig.features;

    if (config is FeatureNavigationConfig) {
      final featureRouter = applicationFeatures[config.id];
      if (featureRouter != null) {
        return featureRouter;
      } else {
        throw Exception(
            'Feature `${config.id}` not supported inside of ${_currentApplicationNavigatorConfig.applicationId} application');
      }
    } else if (config is ScreenNavigationConfig) {
      final featureRouter = applicationFeatures[config.feature];
      if (featureRouter != null) {
        return featureRouter;
      } else {
        throw Exception(
            'Feature `${config.feature}` not supported inside of ${_currentApplicationNavigatorConfig.applicationId} application');
      }
    } else {
      throw Exception('Not supported navigation config: ${config.runtimeType}');
    }
  }

  static const INITIAL_ROUTE_ID = '/';
}
