import 'package:navigation/navigation.dart';

abstract class ScreenNavigationConfig extends NavigationConfig {
  final String feature;

  ScreenNavigationConfig({required String id, required this.feature}): super('$feature#$id');
}