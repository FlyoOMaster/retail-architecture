import 'package:flutter_bloc/flutter_bloc.dart';

import 'core_bloc.dart';

abstract class BaseCubit<T> extends Cubit<T> with CoreBloc {
  BaseCubit(T state): super(state);
}