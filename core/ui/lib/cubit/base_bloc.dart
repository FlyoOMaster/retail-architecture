import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui/cubit/core_bloc.dart';

abstract class BaseBloc<E, S> extends Bloc<E, S> with CoreBloc {
  BaseBloc(S state) : super(state);
}