import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class BasePage<S, T extends BlocBase<S>> extends StatelessWidget {

  const BasePage({Key? key}) : super(key: key);

  T createBloc();

  initBloc(T bloc) {}

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) {
          final cubit = createBloc();
          initBloc(cubit);
          return cubit;
        },
        child: BlocBuilder<T, S>(builder: (context, state) {
          return buildPage(context, getBloc(context), state);
        }));
  }

  Widget buildPage(BuildContext context, T bloc, S state);

  T getBloc(BuildContext context) {
    return context.read<T>();
  }
}