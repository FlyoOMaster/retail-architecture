abstract class UseCase {

}

abstract class FutureUseCase<P, T> extends UseCase {
  Future<T> call(P params);
}

abstract class SimpleFutureUseCase<T> extends UseCase {
  Future<T> call();
}
