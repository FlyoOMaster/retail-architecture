A library for Dart developers.

## Usage

A simple usage example:

```dart
import 'package:environment/environment.dart';

main() {
  var awesome = new Awesome();
}
```

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: http://example.com/issues/replaceme
