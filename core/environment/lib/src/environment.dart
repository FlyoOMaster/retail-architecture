class Environment {
  final ApplicationWorkType applicationWorkType;

  Environment({required this.applicationWorkType});
}

enum ApplicationWorkType {
  host,
  standalone,
  embedded
}
