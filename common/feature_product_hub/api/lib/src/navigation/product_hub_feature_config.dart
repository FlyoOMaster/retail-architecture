import 'package:navigation/navigation.dart';

class ProductHubFeatureConfig extends FeatureNavigationConfig {
  final ProductHubFeatureArgs args;
  ProductHubFeatureConfig(this.args) : super(NAME);

  static const NAME = 'product_hub';
}

class ProductHubFeatureArgs {
  final ProductHubFeatureApplicationType currentApplication;

  ProductHubFeatureArgs({required this.currentApplication});
}

enum ProductHubFeatureApplicationType {
  retail,
  neobroker
}