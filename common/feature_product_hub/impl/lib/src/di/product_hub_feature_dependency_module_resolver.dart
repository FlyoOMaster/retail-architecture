import 'package:di/di.dart';
import 'package:feature_product_hub_api/feature_product_hub_api.dart';
import 'package:feature_product_hub_impl/src/navigation/product_hub_router.dart';
import 'package:feature_product_hub_impl/src/screens/atm/atm_cubit.dart';
import 'package:feature_product_hub_impl/src/screens/hub/product_hub_cubit.dart';
import 'package:navigation/navigation.dart';

class ProductHubFeatureDependencyModuleResolver {
  static register() {
    DependencyProvider.registerLazySingleton<NavigationRouter>(
      () => ProductHubRouter(),
      instanceName: ProductHubFeatureConfig.NAME,
    );

    _cubits();
  }

  static _cubits() {
    DependencyProvider.registerFactory(() => ProductHubCubit(navigator: DependencyProvider.get()));
    DependencyProvider.registerFactory(
      () => AtmHubCubit(
        navigator: DependencyProvider.get(),
      ),
    );
  }
}
