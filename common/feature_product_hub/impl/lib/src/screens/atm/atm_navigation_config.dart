import 'package:feature_product_hub_api/feature_product_hub_api.dart';
import 'package:navigation/navigation.dart';

class AtmNavigationConfig extends ScreenNavigationConfig {
  final String from;

  AtmNavigationConfig({
    required this.from,
  }) : super(
          id: 'atm',
          feature: ProductHubFeatureConfig.NAME,
        );
}
