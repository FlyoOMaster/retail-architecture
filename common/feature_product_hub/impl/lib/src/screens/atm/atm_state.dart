import 'package:equatable/equatable.dart';

class AtmState extends Equatable {
  final List<String> atms;
  final String from;

  AtmState({
    required this.atms,
    required this.from,
  });

  @override
  List<Object?> get props => [atms, from];
}
