import 'package:di/di.dart';
import 'package:feature_product_hub_impl/src/screens/atm/atm_cubit.dart';
import 'package:feature_product_hub_impl/src/screens/atm/atm_navigation_config.dart';
import 'package:feature_product_hub_impl/src/screens/atm/atm_state.dart';
import 'package:feature_product_hub_impl/src/screens/hub/product_hub_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AtmPage extends StatelessWidget {
  final AtmNavigationConfig config;
  const AtmPage({Key? key, required this.config}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => DependencyProvider.get<AtmHubCubit>()..init(config.from),
      child: BlocBuilder<AtmHubCubit, AtmState>(builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: Text('From: ${state.from}'),
          ),
          body: Column(
            children: state.atms
                .map((e) => InkWell(
                      child: Text(e),
                      onTap: () => context.read<AtmHubCubit>().selectAtm(e),
                    ))
                .toList(),
          ),
        );
      }),
    );
  }
}
