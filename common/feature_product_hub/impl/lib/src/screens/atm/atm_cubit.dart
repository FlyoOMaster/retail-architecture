import 'package:feature_product_hub_impl/src/screens/atm/atm_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:navigation/navigation.dart';

class AtmHubCubit extends Cubit<AtmState> {
  final NavigationProvider navigator;
  final List<String> _atms = ['Yas mall', 'Mareena mall'];

  AtmHubCubit({
    required this.navigator,
  }) : super(AtmState(atms: [], from: '')) {
    emit(AtmState(atms: _atms, from: ''));
  }

  init(String from) {
    emit(AtmState(atms: _atms, from: from));
  }

  selectAtm(String atm) {
    navigator.goBack(atm);
  }
}
