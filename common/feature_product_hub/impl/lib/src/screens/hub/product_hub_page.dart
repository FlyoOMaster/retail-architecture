import 'package:di/di.dart';
import 'package:feature_product_hub_api/feature_product_hub_api.dart';
import 'package:feature_product_hub_impl/src/screens/hub/product_hub_cubit.dart';
import 'package:feature_product_hub_impl/src/screens/hub/product_hub_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ui/page/base_page.dart';

class ProductHubPage extends BasePage<ProductHubState, ProductHubCubit> {
  final ProductHubFeatureArgs args;

  ProductHubPage({Key? key, required this.args}): super(key: key);

  @override
  createBloc() => DependencyProvider.get<ProductHubCubit>();

  @override
  initBloc(ProductHubCubit cubit) {
    cubit.init(args);
  }

  @override
  Widget buildPage(BuildContext context, ProductHubCubit cubit, ProductHubState state) {
    final retailClick = cubit.navigateToRetail;
    final neobrokerClick = cubit.navigateToNeobroker;
    return Scaffold(
      appBar: AppBar(title: Text('Product hub'),),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(onPressed: state.retailEnabled ? retailClick : null, child: Text('Retail')),
          ElevatedButton(onPressed: state.neobrokerEnabled ? neobrokerClick : null, child: Text('Neobroker'))
        ],
      ),
    );
  }


}
