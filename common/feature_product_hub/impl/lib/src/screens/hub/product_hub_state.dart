import 'package:equatable/equatable.dart';

class ProductHubState extends Equatable {
  final bool retailEnabled;
  final bool neobrokerEnabled;

  ProductHubState({
    required this.retailEnabled,
    required this.neobrokerEnabled,
  });

  factory ProductHubState.empty() => ProductHubState(
    retailEnabled: false,
    neobrokerEnabled: false
  );

  ProductHubState copyWith({bool? retailEnabled, bool? neobrokerEnabled}) {
    return ProductHubState(
      retailEnabled: retailEnabled ?? this.retailEnabled,
      neobrokerEnabled: neobrokerEnabled ?? this.neobrokerEnabled,
    );
  }

  @override
  List<Object?> get props => [retailEnabled, neobrokerEnabled];
}


