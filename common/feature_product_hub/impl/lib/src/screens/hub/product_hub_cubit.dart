import 'package:feature_product_hub_api/feature_product_hub_api.dart';
import 'package:feature_product_hub_impl/src/screens/atm/atm_navigation_config.dart';
import 'package:feature_product_hub_impl/src/screens/hub/product_hub_state.dart';
import 'package:feature_welcome_screen_api/feature_welcome_screen_api.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:navigation/navigation.dart';
import 'package:neobroker_feature_welcome_screen_api/feature_welcome_screen_api.dart';
import 'package:ui/cubit/base_cubit.dart';

class ProductHubCubit extends BaseCubit<ProductHubState> {
  final ApplicationNavigationProvider navigator;

  ProductHubCubit({
    required this.navigator,
  }) : super(ProductHubState.empty());

  init(ProductHubFeatureArgs args) {
    var currentApplication = args.currentApplication;
    emit(state.copyWith(
        retailEnabled:
            currentApplication != ProductHubFeatureApplicationType.retail,
        neobrokerEnabled:
            currentApplication != ProductHubFeatureApplicationType.neobroker));
  }

  navigateToRetail() async {
    navigator.replaceApplication(ApplicationType.retail, WelcomeScreenFeatureConfig());
  }

  navigateToNeobroker() async {
    navigator.replaceApplication(ApplicationType.neobroker, NeobrokerWelcomeScreenFeatureConfig());
  }
}
