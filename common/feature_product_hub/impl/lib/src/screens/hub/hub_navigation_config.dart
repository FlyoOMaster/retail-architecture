import 'package:feature_product_hub_api/feature_product_hub_api.dart';
import 'package:navigation/navigation.dart';

class ProductHubNavigationConfig extends ScreenNavigationConfig {
  final ProductHubFeatureArgs args;

  ProductHubNavigationConfig(this.args)
      : super(
          id: 'hub',
          feature: ProductHubFeatureConfig.NAME,
        );
}
