import 'package:feature_product_hub_api/feature_product_hub_api.dart';
import 'package:feature_product_hub_impl/src/screens/atm/atm_navigation_config.dart';
import 'package:feature_product_hub_impl/src/screens/atm/atm_page.dart';
import 'package:feature_product_hub_impl/src/screens/hub/hub_navigation_config.dart';
import 'package:feature_product_hub_impl/src/screens/hub/product_hub_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';
import 'package:navigation/navigation.dart';

class ProductHubRouter extends NavigationRouter {
  @override
  Route getScreenRoute(RouteSettings settings) {
    final config = settings.arguments;
    switch(config.runtimeType) {
      case ProductHubFeatureConfig:
      case ProductHubNavigationConfig:
        final ProductHubFeatureArgs args;
        if(config is ProductHubFeatureConfig) {
          args = config.args;
        } else if (config is ProductHubNavigationConfig) {
          args = config.args;
        } else {
          throw Exception('ProductHubRouter: not supported config: $config');
        }
        return ProductHubPage(args: args).toRoute();
      case AtmNavigationConfig:
        return AtmPage(config: config as AtmNavigationConfig).toRoute();

      default: throw Exception('Unknown $config for the ${this.runtimeType}');
    }
  }

}

extension _RouteWrapper on Widget {
  MaterialPageRoute toRoute() {
    return MaterialPageRoute(builder: (BuildContext context) => this);
  }
}